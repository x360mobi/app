export default {
	name: 'sidebar',
	created () {
	},
	mounted () {
	},
	data () {
		return {
			title: process.env.APP_TITLE,
			show_sidebar: false
		}
	},
	methods: {
		toggleSidebar () {
			let sidebar = document.getElementsByClassName('sidebar-wrapper')[0],
				toggler = document.getElementsByClassName('sidebar-toggler')[0]
			this.show_sidebar ? sidebar.classList.remove('open-sidebar') : sidebar.classList.add('open-sidebar')
			this.show_sidebar ? toggler.classList.remove('active') : toggler.classList.add('active')
			this.show_sidebar = !this.show_sidebar
		},
		logout () {
			this
				.$store
				.dispatch('logout')
				.then(response => {
					this.$router.push('/login')
				})
		}
	}
}
