export default {
	name: 'error-box',
	created () {
	},
	mounted () {
		setTimeout(() => { this.opacity = 0 }, 5000)
	},
	props: [ 'message' ],
	data () {
		return {
			opacity: 1
		}
	},
	methods: {

	}
}