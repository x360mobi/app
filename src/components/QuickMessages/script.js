import axios from '../../axios'
import * as moment from 'moment'

export default {
	name: 'quick-messages',
	created () {
	},
	mounted () {
		this.getMasks()
		this.getQuickMessages()
	},
	data () {
		return {
			masks: null,
			send_quick: false,
			message: {
				phone: null,
				mask_id: null,
				mask: null,
				message: null
			},
			message_history: null,
			loader: false,
			error: false,
			errors: {
				fetch: "An error occurred. Balance records could not be fetched"
			},
			error_message: null,
			loading: false,
			send_error: null,
			not_sent: false,
			from: null,
			to: null,
		}
	},
	methods: {
		getMasks () {
			axios
				.get('mask', { params: {company_id: this.$store.getters.company._id} })
				.then(res => {
					this.masks = res.data
				})
		},
		sendMessage (e) {
			e.preventDefault()
			let mask = null
			if (!this.message.message)
				alert('Please enter a message before you proceed.')

			if (!this.message.mask_id)
				alert('Please select a mask.')
			else
				mask = _.find(this.masks, mask => { return mask._id == this.message.mask_id })
			axios
				.get('send-quick', { params: {
						mask: mask.name,
						number: this.message.phone,
						message: this.message.message,
						mask_id: this.message.mask_id,
						log_history: true,
						company_id: this.$store.getters.company._id
					}
				})
				.then(res => {
					this.not_sent = false
					this.send_quick = false
					this.getQuickMessages()

				})
				.catch(err => {
					this.not_sent = true
					this.send_error = 'An error occured. The message could not be sent.'
				})
		},
		getQuickMessages () {
			axios
				.get('get-quick', { params: { company_id: this.$store.getters.company._id } })
				.then(res => {
					this.message_history = res.data.reverse()
					_.forEach(this.message_history, (message, index) => {
						this.message_history[index].created_at = moment(message.created_at).format('hh:mmA - MMM D, YYYY')
					})
				})
		},
		getQuickMessagesLogs () {
			let base_url = `${process.env.API_PROTOCOL}://api.${process.env.APP_NAME}.${process.env.APP_EXTENSION}/${process.env.API_VERSION}`,
				token = localStorage.getItem(process.env.TOKEN_KEY);
			this.to = this.to ? this.to : moment().format('YYYY-MM-DD');
			window.location.href = `${base_url}/get-quick-logs?token=${token}&company_id=${this.$store.getters.company._id}&to=${this.to}&from=${this.from}`;
		}
	}
}