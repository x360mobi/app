import axios from '../../axios'
import * as moment from 'moment'
import * as _ from 'lodash'

export default {
	name: 'home',
	created () {
	},
	mounted () {
		this.fetchCampaigns()
		this.getQuickMessages()
	},
	data () {
		return {
			params: {
				p: 1,
				w: 'user,mask,baseList',
				limit: 5,
				company_id: this.$store.getters.company._id
			},
			message_history: null,
			campaigns: null,
			error: false,
			errors: {
				fetch: "An error occurred. Campaigns could not be fetched",
				deletion: "An error occurred. The campaign could not be deleted."
			},
			error_message: null
		}
	},
	methods: {
		fetchCampaigns () {
			axios
				.get('/campaign', { params: this.params })
				.then(response => {
					this.campaigns = response.data
					_.forEach(this.campaigns, campaign => {
						campaign.date = moment(campaign.date).format('MMM D, YYYY')
						campaign.start_time = moment(campaign.start_time).format('hh:mma')
						campaign.end_time = moment(campaign.end_time).format('hh:mma')
					})
				})
				.catch(err => {
					this.error = true
					this.error_message = this.errors.fetch
				})
		},
		getQuickMessages () {
			axios
				.get('get-quick', { params: { company_id: this.$store.getters.company._id, limit: 5 } })
				.then(res => {
					this.message_history = res.data.reverse()
					_.forEach(this.message_history, (message, index) => {
						this.message_history[index].created_at = moment(message.created_at).format('hh:mmA - MMM D, YYYY')
					})
				})
		}
	}
}