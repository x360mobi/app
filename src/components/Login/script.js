export default {
	name: 'login',
	created () {
		if (this.$store.getters.isLoggedIn)
			this.$router.push('/')
	},
	mounted () {
		document.getElementsByClassName('row')[0].setAttribute('style', `height: ${window.innerHeight}px;`)
	},
	data () {
		return {
			processing: false,
			disabled: false,
			login_failed: false,
			username: '',
			password: ''
		}
	},
	methods: {
		login () {
			this.toggleButtonStates()

			this
				.$store
				.dispatch('login', {username: this.username, password: this.password})
				.then(response => {
					if (this.$store.getters.isLoggedIn) {
						this.toggleButtonStates()
						this.login_failed = false
						this.$router.push('/')
					}
				})
				.catch(err => {
					this.login_failed = true
					this.toggleButtonStates()
					this.username = ''
					this.password = ''
					setTimeout(() => this.login_failed = false, 3000)
				})
		},
		toggleButtonStates () {
			this.processing = !this.processing
			this.disabled = !this.disabled
		}
	}
}
