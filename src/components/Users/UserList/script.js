import axios from '../../../axios'
import * as moment from 'moment'
import * as _ from 'lodash'

export default {
	name: 'users',
	mounted () {
		this.fetchUsers()
	},
	data () {
		return {
			users: null,
			params: {
				p: 1,
				role: 0
			},
			error: false,
			errors: {
				fetch: "An error occurred. Users could not be fetched",
				deletion: "An error occurred. The user could not be deleted."
			},
			error_message: null
		}
	},
	methods: {
		fetchUsers () {
			axios
				.get('/user', { params: this.params })
				.then(response => {
					this.users = response.data
					_.forEach(this.users, user => {
						let created_at = moment.utc(user.created_at).toDate()
						user.created_at = moment(created_at).format('MMM D, YYYY')

						if (user.hasOwnProperty('last_login')) {
							let utc = moment.utc(user.last_login).toDate()
							user.last_login = moment(utc).format('MMM D, YYYY hh:mmA')
						}
					})
				})
				.catch(err => {
					this.error = true
					this.error_message = this.errors.fetch
				})
		},
		deleteUser (id) {
			let confirmation = confirm("Are you sure you want to delete this user? All the associated information would also be deleted.")
			if (confirmation) {
				axios
					.delete(`/user/${id}`)
					.then(res => {
						this.users = _.filter(this.users, user => { return user._id !== id })
					})
					.catch(err => {
						this.error = true
						this.error_message = this.errors.deletion
					})
			}
		}
	}
}