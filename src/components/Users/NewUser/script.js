import MaskedInput from 'vue-text-mask'
import axios from '../../../axios'

export default {
	name: 'new-user',
	created () {
	},
	mounted () {
	},
	components: {
		MaskedInput
	},
	data () {
		return {
			user: {
				first_name: null,
				last_name: null,
				email: null,
				phone: null,
				role: 0,
				password: null
			},
			confirm_password: null,
			loading: false,
			invalid: true,
			error: false
		}
	},
	methods: {
		validate () {
				this.invalid = !(this.user.password === this.confirm_password)
		},
		addUser () {
			this.loading = true
			this.user.phone = this.user.phone.replace(/\(|\)|\s/g, '')

			axios
				.post('user', this.user)
				.then(res => {
					this.$router.push({name: 'UserList'})
				})
				.catch(err => {
					this.error = true
				})
		}
	}
}