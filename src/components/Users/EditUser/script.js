import MaskedInput from 'vue-text-mask'
import axios from '../../../axios'

export default {
	name: 'edit-user',
	created () {
	},
	mounted () {
		this.id = this.$route.params.id
		this.getUser()
	},
	components: {
		MaskedInput
	},
	data () {
		return {
			id: null,
			user: {
				first_name: null,
				last_name: null,
				email: null,
				phone: null,
				role: 0,
				password: null
			},
			confirm_password: null,
			loading: false,
			invalid: false,
			error: false
		}
	},
	methods: {
		getUser () {
			axios
				.get(`user/${this.id}`)
				.then(res => {
					let user = res.data
					this.user.first_name = user.first_name
					this.user.last_name = user.last_name
					this.user.email = user.email
					this.user.phone = `(${user.phone.substring(0, 3)}) ${user.phone.substring(3, 11)}`
				})
		},
		validate () {
			this.invalid = !(this.user.password === this.confirm_password)
		},
		editUser () {
			this.loading = true
			this.user.phone = this.user.phone.replace(/\(|\)|\s/g, '')
			if (!this.user.password)
				delete this.user.password;

			axios
				.put(`user/${this.id}`, this.user)
				.then(res => {
					this.$router.push({name: 'UserList'})
				})
				.catch(err => {
					this.loading = false
					this.error = true
				})
		}
	}
}