import axios from '../../../axios'

export default {
	name: 'edit-base',
	created () {
	},
	mounted () {
		this.id = this.$route.params.id
		this.getBase()
	},
	components: {
	},
	data () {
		return {
			id: null,
			base: {
				name: null,
				file: null,
				update: false
			},
			loading: false,
			error: false
		}
	},
	methods: {
		getBase () {
			axios
				.get(`base/${this.id}`)
				.then(res => {
					this.base = res.data
				})
		},
		editBase () {
			this.loading = true
			let form_data = new FormData()
			form_data.append('id', this.id)
			form_data.append('name', this.base.name)
			form_data.append('file', this.base.file)
			form_data.append('update', this.base.update)

			axios
				.post(`base`, form_data, { headers: { 'content-type': 'multipart/form-data' } })
				.then(res => {
					this.$router.push({name: 'BaseList'})
				})
				.catch(err => {
					this.loading = false
					this.error = true
				})
		},
		fileSelected (e) {
			let file = e.target.files[0]
			this.base.file = file
		}
	}
}