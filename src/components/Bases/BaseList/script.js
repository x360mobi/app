import axios from '../../../axios'
import * as moment from 'moment'
import * as _ from 'lodash'

export default {
	name: 'base-list',
	mounted () {
		this.fetchBases()
	},
	data () {
		return {
			bases: [],
			params: {
				p: 1,
				w: 'user'
			},
			error: false,
			errors: {
				fetch: "An error occurred. Base lists could not be fetched",
				deletion: "An error occurred. The base could not be deleted."
			},
			error_message: null
		}
	},
	methods: {
		fetchBases () {
			axios
				.get('/base', { params: this.params })
				.then(response => {
					this.bases = response.data
					_.forEach(this.bases, base => {
						base.created_at = moment(base.created_at).format('MMM D, YYYY')

						if (base.hasOwnProperty('updated_at')) {
							base.updated_at = moment(base.updated_at).format('MMM D, YYYY')
						}
					})
				})
				.catch(err => {
					this.error = true
					this.error_message = this.errors.fetch
				})
		},
		deleteBase (id) {
			let confirmation = confirm('Are you sure you want to delete this base? All the related data will be lost.')
			if (confirmation)
				axios
					.delete(`base/${id}`)
					.then(res => {
						this.bases = _.filter(this.bases, base => { return base._id !== id })
					})
					.catch(err => {
						this.error = true
					})
		},
		downloadBase (event, id) {
			if (event.target.value) {
				let base_url = `${process.env.API_PROTOCOL}://api.${process.env.APP_NAME}.${process.env.APP_EXTENSION}/${process.env.API_VERSION}`,
					token = localStorage.getItem(process.env.TOKEN_KEY);
				window.location.href = `${base_url}/download-base?token=${token}&id=${id}&vendor=${event.target.value}`;
			}
		}
	}
}