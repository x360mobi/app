import axios from '../../../axios'

export default {
	name: 'new-base',
	created () {
	},
	mounted () {
	},
	components: {
	},
	data () {
		return {
			base: {
				name: null,
				file: null
			},
			loading: false,
			error: false
		}
	},
	methods: {
		addBase () {
			this.loading = true
			let form_data = new FormData()
			form_data.append('name', this.base.name)
			form_data.append('file', this.base.file)
			form_data.append('company_id', this.$store.getters.company._id)

			axios
				.post('base', form_data, {  })
				.then(res => {
					this.$router.push({name: 'BaseList'})
				})
				.catch(err => {
					this.error = true
					this.loading = false
				})
		},
		fileSelected (e) {
			let file = e.target.files[0]
			this.base.file = file
		}
	}
}