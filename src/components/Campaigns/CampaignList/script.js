import axios from '../../../axios'
import * as moment from 'moment'
import * as _ from 'lodash'

export default {
	name: 'campaign-list',
	mounted () {
		this.fetchCampaigns()
	},
	data () {
		return {
			campaigns: [],
			params: {
				p: 1,
				w: 'user,mask,baseList',
				company_id: this.$store.getters.company._id
			},
			error: false,
			errors: {
				fetch: "An error occurred. Campaigns could not be fetched",
				deletion: "An error occurred. The campaign could not be deleted."
			},
			error_message: null
		}
	},
	methods: {
		fetchCampaigns () {
			axios
				.get('/campaign', { params: this.params })
				.then(response => {
					this.campaigns = response.data
					_.forEach(this.campaigns, campaign => {
						campaign.date = moment(campaign.date).format('MMM D, YYYY')
						campaign.start_time = moment(campaign.start_time).format('hh:mma')
						campaign.end_time = moment(campaign.end_time).format('hh:mma')
					})
				})
				.catch(err => {
					this.error = true
					this.error_message = this.errors.fetch
				})
		},
		deleteCampaign (id) {
			let confirmation = confirm('Are you sure you want to delete this campaign? All the related data will be lost.')
			if (confirmation)
				axios
					.delete(`campaign/${id}`)
					.then(res => {
						this.campaigns = _.filter(this.campaigns, campaign => { return campaign._id !== id })
					})
					.catch(err => {
						this.error = true
					})
		}
	}
}