import axios from '../../../axios'
import * as _ from 'lodash'
import * as moment from 'moment'

export default {
	name: 'new-campaign',
	created () {
	},
	mounted () {
		this.getMasks()
		this.getBases()
	},
	components: {
	},
	data () {
		return {
			campaign: {
				name: null,
				date: null,
				start_time: null,
				end_time: null,
				mask_id: null,
				base_list_id: null,
				message: null,
				notes: null,
				type: null
			},
			start_times: [
				'00:00', '00:30', '01:00', '01:30', '02:00', '02:30', '03:00', '03:30', '04:00', '04:30', '05:00', '05:30', '06:00', '06:30',
				'07:00', '07:30', '08:00', '08:30', '09:00', '09:30', '10:00', '10:30', '11:00', '11:30', '12:00', '12:30', '13:00', '13:30',
				'14:00', '14:30', '15:00', '15:30', '16:00', '16:30', '17:00', '17:30', '18:00', '18:30', '19:00', '19:30', '20:00', '20:30',
				'21:00', '21:30', '22:00', '22:30', '23:00', '23:30',
			],
			end_times: [
				'00:00', '00:30', '01:00', '01:30', '02:00', '02:30', '03:00', '03:30', '04:00', '04:30', '05:00', '05:30', '06:00', '06:30',
				'07:00', '07:30', '08:00', '08:30', '09:00', '09:30', '10:00', '10:30', '11:00', '11:30', '12:00', '12:30', '13:00', '13:30',
				'14:00', '14:30', '15:00', '15:30', '16:00', '16:30', '17:00', '17:30', '18:00', '18:30', '19:00', '19:30', '20:00', '20:30',
				'21:00', '21:30', '22:00', '22:30', '23:00', '23:30',
			],
			masks: null,
			bases: [],
			loading: false,
			error: false,
			not_sent: false,
			send_error: null,
			next: false,
			phone: null,
			toggle_base: false,
			base: {
				name: null,
				file: null
			}
		}
	},
	methods: {
		addCampaign () {
			this.loading = true
			this.campaign.start_time = this.campaign.date + ' ' + this.campaign.start_time + ':00'
			this.campaign.end_time = this.campaign.date + ' ' + this.campaign.end_time + ':00'
			this.campaign.company_id = this.$store.getters.company._id

			axios
				.post('campaign', this.campaign)
				.then(res => {
					this.$router.push({name: 'CampaignList'})
				})
				.catch(err => {
					this.error = true
					this.loading = false
				})
		},
		getMasks () {
			axios
				.get('mask', { params: {company_id: this.$store.getters.company._id} })
				.then(res => {
					this.masks = res.data
				})
		},
		getBases () {
			axios
				.get('base', { params: {company_id: this.$store.getters.company._id} })
				.then(res => {
					this.bases = res.data
				})
		},
		filterEndTimes () {
			this.end_times = _.filter(this.start_times, time => {
				return time > this.campaign.start_time
			})
		},
		sendMessage (e) {
			e.preventDefault()
			let mask = null
			if (!this.campaign.message)
				alert('Please enter a message before you proceed.')

			if (!this.campaign.mask_id)
				alert('Please select a mask.')
			else
				mask = _.find(this.masks, mask => { return mask._id == this.campaign.mask_id })
			axios
				.get('send-quick', { params: { mask: mask.name, number: this.phone, message: this.campaign.message } })
				.then(res => {
					this.not_sent = false
				})
				.catch(err => {
					this.not_sent = true
					this.send_error = 'An error occured. The message could not be sent.'
				})
		},
		fileSelected (e) {
			let file = e.target.files[0]
			this.base.file = file
		},
		addBase () {
			this.loading = true
			let form_data = new FormData()
			form_data.append('name', this.base.name)
			form_data.append('file', this.base.file)
			form_data.append('company_id', this.$store.getters.company._id)

			axios
				.post('base', form_data, { headers: { 'content-type': 'multipart/form-data' } })
				.then(res => {
					this.loading = false
					this.toggle_base = false
					this.getBases()
				})
				.catch(err => {
					this.error = true
					this.loading = false
				})
		}
	}
}