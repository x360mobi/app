import axios from 'axios'

const http = axios.create({
	baseURL: `${process.env.API_PROTOCOL}://api.${process.env.APP_NAME}.${process.env.APP_EXTENSION}/${process.env.API_VERSION}`
})

const token = localStorage.getItem(process.env.TOKEN_KEY)

if (token) {
	http.defaults.headers.common['Authorization'] = `Bearer ${token}`
}

export default http
