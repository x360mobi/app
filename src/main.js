import Vue from 'vue'

import App from './App'
import router from './router'
import store from './store'

import Sidebar from '@/components/Shared/Sidebar/Sidebar'
import ErrorBox from '@/components/Shared/ErrorBox/ErrorBox'

Vue.component('sidebar', Sidebar)
Vue.component('error-box', ErrorBox)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
