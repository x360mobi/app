import Vue from 'vue'
import Router from 'vue-router'

import Login from '@/components/Login/Login'
import Home from '@/components/Home/Home'

import Users from '@/components/Users/Users'
import UserList from '@/components/Users/UserList/UserList'
import NewUser from '@/components/Users/NewUser/NewUser'
import EditUser from '@/components/Users/EditUser/EditUser'

import Campaigns from '@/components/Campaigns/Campaigns'
import CampaignList from '@/components/Campaigns/CampaignList/CampaignList'
import NewCampaign from '@/components/Campaigns/NewCampaign/NewCampaign'
import EditCampaign from '@/components/Campaigns/EditCampaign/EditCampaign'

import Bases from '@/components/Bases/Bases'
import BaseList from '@/components/Bases/BaseList/BaseList'
import NewBase from '@/components/Bases/NewBase/NewBase'
import EditBase from '@/components/Bases/EditBase/EditBase'

import QuickMessages from '@/components/QuickMessages/QuickMessages'

Vue.use(Router)

let router = new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/users',
      component: Users,
      children: [
				{
					path: '/',
					name: 'UserList',
					component: UserList
				},
        {
          path: 'new-user',
          name: 'NewUser',
          component: NewUser
        },
				{
					path: ':id',
					name: 'editUser',
					component: EditUser
				}
      ]
    },
		{
			path: '/campaigns',
			component: Campaigns,
			children: [
				{
					path: '/',
					name: 'CampaignList',
					component: CampaignList
				},
				{
					path: 'new-campaign',
					name: 'NewCampaign',
					component: NewCampaign
				},
				{
					path: ':id',
					name: 'EditCampaign',
					component: EditCampaign
				}
			]
		},
		{
			path: '/bases',
			component: Bases,
			children: [
				{
					path: '/',
					name: 'BaseList',
					component: BaseList
				},
				{
					path: 'new-base',
					name: 'NewBase',
					component: NewBase
				},
				{
					path: ':id',
					name: 'EditBase',
					component: EditBase
				}
			]
		},
		{
			path: '/quick-messages',
			name: 'QuickMessages',
			component: QuickMessages
		},
	],
  mode: 'history'
})

router.beforeEach((to, from, next) => {
  if (to.name !== 'Login') {
		localStorage.getItem('user') ? next() : next('/login')
	} else {
    next()
  }
})

export default router
