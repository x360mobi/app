import http from '../../axios'

const userKey = process.env.USER_KEY
const tokenKey = process.env.TOKEN_KEY
const companyKey = process.env.COMPANY_KEY

const state = {
	user: JSON.parse(localStorage.getItem(userKey)) || {},
	company: JSON.parse(localStorage.getItem(companyKey)) || {}
}

const getters = {
	isLoggedIn: (state) => state.user && state.user.hasOwnProperty('token'),
	getToken: (state) => state.user ? state.user.token : null,
	user: (state) => state.user,
	company: (state) => state.company
}

const mutations = {
	set: (state, user) => {
		localStorage.setItem(userKey, JSON.stringify(user))
		localStorage.setItem(tokenKey, user.token)
		state.user = user
		http.defaults.headers.common['Authorization'] = `Bearer ${state.user.token}`
	},
	setCompany: (state, company) => {
		localStorage.setItem(companyKey, JSON.stringify(company))
		state.company = company
		delete state.user.company
	}
}

const actions = {
	login: ({commit, state}, credentials) => {
		return new Promise((resolve, reject) => {
			http
				.post('/authenticate', credentials)
				.then(response => {
					let user = response.data
					if (user.company) {
            commit('set', user)
            commit('setCompany', user.company)
            resolve(user)
					} else {
						reject("You need client credentials to login.")
					}
				})
				.catch(err => {
					reject(err)
				})
		})
	},
	logout: ({commit, state}) => {
		return new Promise((resolve, reject) => {
			localStorage.removeItem(userKey)
			localStorage.removeItem(tokenKey)
			resolve()
		})
	}
}

export default {
	state,
	getters,
	mutations,
	actions
}
