module.exports = {
	NODE_ENV: '"production"',
	USER_KEY: '"user"',
	TOKEN_KEY: '"token"',
	COMPANY_KEY: '"company"',
	APP_NAME: '"sms360"',
	APP_EXTENSION: '"mobi"',
	APP_TITLE: '"sms360"',
	API_VERSION: '"v1"',
	API_PROTOCOL: '"http"',
}
