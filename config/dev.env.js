var merge = require('webpack-merge')
var prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
	NODE_ENV: '"development"',
	API_PROTOCOL: '"http"',
	APP_EXTENSION: '"test"'
})
